
#include <GL\glew.h>
#include <GL\freeglut.h>

#include <iostream>

void _Display();
void _Reshape(int w, int h);
void _Keyboard(unsigned char key, int x, int y);
void _Mouse(int button, int state, int x, int y);

int armY = 0, shoulderAngle = 0, elbowAngle = 0, wristAngle = 0, clawAngle = 90;

bool Init()
{
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);

	glutInitWindowSize(900, 900);
	glutInitWindowPosition(300, 10);
	glutCreateWindow("Simpler");

	glutDisplayFunc(_Display);
	glutKeyboardFunc(_Keyboard);
	glutReshapeFunc(_Reshape);
	glutMouseFunc(_Mouse);

	if(glewInit() != GLEW_OK)
		return false;

	glClearColor(0.2f, 0.2f, 0.2f, 0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	//glPolygonMode(GL_BACK, GL_LINE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(65.0, 1.0, 1.0, 10.0);

	return true;
}

void Update()
{
}

void _Display()
{
	Update();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//model transforms

	//arm ---------------------
	glPushMatrix();

	glTranslatef(-1.0f, 1.0f, -5.0f);
	glRotatef(armY, 0, 1.0f, 0);
	glRotatef(-60.0f + shoulderAngle, 0, 0, 1.0f);

	//shoulder
	glutWireSphere(0.4, 12, 12);

		//upper arm
		glPushMatrix();

			glTranslatef(1.0f, 0, 0);
			glScalef(4.0f, 1.0f, 1.0f);

			//upper arm box
			glutWireCube(0.5); 

		glPopMatrix();

		glTranslatef(2.0f, 0, 0);
		glRotatef(40.0f + elbowAngle, 0, 0, 1.0f);

		//elbow
		glutWireSphere(0.3, 10, 10); 

		//lower arm
		glPushMatrix();

			glScalef(3.2f, 1.0f, 1.0f);
			glTranslatef(0.25f, 0, 0);

			//lower arm box
			glutWireCube(0.5); 

		glPopMatrix();

		glTranslatef(1.7f, 0, 0);
		glRotatef(wristAngle, 0, 0, 1.0f);

		//wrist
		glutWireSphere(0.2, 8, 8);

		glPushMatrix();

			glRotatef(clawAngle/2, 0, 0, 1.0f);
			glTranslatef(0.4f, 0, 0);
			glScalef(1.2f, 0.1f, 1.0f);

			glutWireCube(0.5);

		glPopMatrix();

		glPushMatrix();

			glRotatef(-clawAngle/2, 0, 0, 1.0f);
			glTranslatef(0.4f, 0, 0);
			glScalef(1.2f, 0.1f, 1.0f);

			glutWireCube(0.5);

		glPopMatrix();

	glPopMatrix();
	//arm end -----------------

	glutSwapBuffers();
}

void _Reshape(int w, int h)
{
	if(w < h)
		glViewport(0, 0, w, w);
	else
		glViewport(0, 0, h, h);
}

void _Keyboard(unsigned char key, int x, int y)
{
	int d = 2;

	switch(key)
	{
		case 'w':
			shoulderAngle += d;
		break;

		case 's':
			shoulderAngle -= d;
		break;

		case 'a':
			elbowAngle += d;
		break;

		case 'd':
			elbowAngle -= d;
		break;

		case 'r':
			wristAngle += d;
		break;

		case 'f':
			wristAngle -= d;
		break;

		case 'q':
			armY += d;
		break;

		case 'e':
			armY -= d;
		break;

		case 't':
			clawAngle += d;
		break;

		case 'g':
			clawAngle -= d;
		break;
	}

	glutPostRedisplay();
}

void _Mouse(int button, int state, int x, int y)
{
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	if(!Init())
		return false;

	glutMainLoop();

	return 0;
}