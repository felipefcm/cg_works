
#version 330

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

out vec3 transformedNormal;
out vec4 worldPosition;
out vec2 textureCoords;

uniform mat4 worldToViewMatrix = mat4(1.0);
uniform mat4 projectionMatrix = mat4(1.0);
uniform mat4 objectToWorldMatrix = mat4(1.0);

void main()
{
	worldPosition = objectToWorldMatrix * position;
	
	transformedNormal = normalize( mat3(transpose(inverse(objectToWorldMatrix))) * normal );
	//transformedNormal = normalize( mat3(objectToWorldMatrix) * normal );
	
	textureCoords = texCoord;
	
	gl_Position = projectionMatrix * (worldToViewMatrix * worldPosition);
}