
#pragma once

#include "StdMath.h"

namespace CGFramework
{

using Math::Matrix4;
using Math::Vector3;

class MatrixStack
{

public:
	MatrixStack();

	void Push(const Matrix4& m);
	Matrix4 Pop();
	Matrix4 Peek() const;

	void Clear();

	void Translate(const Vector3&);
	void Scale(const Vector3&);
	void Rotate(float angleDeg, const Vector3& axis);

	void Multiply(const Matrix4&);

private:
	std::stack<Matrix4> m_Stack;

};

}