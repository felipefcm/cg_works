
#include "precompiled.h"

#include "TextureBuffer.h"

using namespace CGFramework;

TextureBuffer::TextureBuffer(GLuint samplerUniform) : m_TexUnit(0), m_SamplerUniform(samplerUniform)
{
}

bool TextureBuffer::Init(TexType type, const char* texFile, GLuint texUnit)
{
	GLfloat maxAniso = 0.0f;
	void* texData = NULL;
	int w = 0, h = 0;

	m_TexUnit = texUnit;
	m_TexType = type;

	glGenTextures(1, &m_BufferHandle);
	glGenSamplers(1, &m_SamplerHandle);

	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);

	IO::Print("	loading texture data");

	texData = IO::ReadTgaImage(texFile, &w, &h);

	IO::CheckGLError();

	Bind();

	glSamplerParameteri(m_SamplerHandle, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glSamplerParameteri(m_SamplerHandle, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glSamplerParameteri(m_SamplerHandle, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glSamplerParameteri(m_SamplerHandle, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glSamplerParameterf(m_SamplerHandle, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);

	glUniform1i(m_SamplerUniform, m_TexUnit);

	//TODO: support other texture types
	glTexImage2D(m_TexType, 0, GL_RGB8, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, texData); 

	Unbind();

	delete texData;

	return true;
}

void TextureBuffer::Bind() const
{
	glActiveTexture(GL_TEXTURE0 + m_TexUnit);
	glBindTexture(m_TexType, m_BufferHandle);
	glBindSampler(0, m_SamplerHandle);
}

void TextureBuffer::Unbind() const
{
	glBindSampler(0, 0);
	glBindTexture(m_TexType, 0);
}

const GLuint TextureBuffer::GetTexUnit() const
{
	return m_TexUnit;
}


