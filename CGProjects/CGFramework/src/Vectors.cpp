
#include "precompiled.h"

#include "Vectors.h"

using namespace CGFramework;
using namespace Math;

void Vector3::Normalize()
{
	float size = sqrtf(x*x + y*y + z*z);

	x /= size;
	y /= size;
	z /= size;
}

void Vector4::Normalize()
{
	float size = sqrtf(x*x + y*y + z*z + w*w);

	x /= size;
	y /= size;
	z /= size;
	w /= size;
}

float Vector3::Dot(const Vector3& a, const Vector3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

Vector3 Vector3::Cross(const Vector3& a, const Vector3& b)
{
	return Vector3(
			a.y * b.z - b.y * a.z,
			-a.x * b.z + b.x * a.z,
			a.x * b.y - b.x * a.y
		);
}

Vector3 Vector3::Interpolate(const Vector3& v1, const Vector3& v2, float alpha)
{
	return Vector3(
			v1*alpha + v2*(1 - alpha)
		);
}

Vector3 Math::operator+ (const Vector3& a, const Vector3& b)
{
	return Vector3(
			a.x + b.x,
			a.y + b.y,
			a.z + b.z
		);
}

Vector3 Math::operator- (const Vector3& a, const Vector3& b)
{
	return Vector3(
			a.x - b.x,
			a.y - b.y,
			a.z - b.z
		);
}

Vector3 Math::operator* (const Vector3& a, const float& b)
{
	return Vector3(
			a.x * b,
			a.y * b,
			a.z * b
		);
}


Vector4 Math::operator+ (const Vector4& a, const Vector4& b)
{
	return Vector4(
			a.x + b.x,
			a.y + b.y,
			a.z + b.z,
			a.w + b.w
		);
}

Vector4 Math::operator- (const Vector4& a, const Vector4& b)
{
	return Vector4(
			a.x - b.x,
			a.y - b.y,
			a.z - b.z,
			a.w - b.w
		);
}

Vector4 Math::operator* (const Vector4& a, const float& b)
{
	return Vector4(
			a.x * b,
			a.y * b,
			a.z * b,
			a.w * b
		);
}


