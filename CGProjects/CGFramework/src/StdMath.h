
/*
	Standard human math
*/

#pragma once

namespace CGFramework
{

static const float PI = 3.141592653589f;
static const float HALF_PI = PI * 0.5f;
static const float QUARTER_PI = PI * 0.25f;

static const float E = 2.7182818284590f;

static const float EPSILON = 0.01f;

static const float GOLDEN = 1.61803398874989f;

}

#include "Matrix.h"
#include "Vectors.h"
#include "Quaternion.h"
