
#include "precompiled.h"

#include "Quaternion.h"
#include "StdMath.h"

using namespace CGFramework;
using namespace Math;

Quaternion::Quaternion() : x(0), y(0), z(0), w(1.0f) //that gives us an identity rotation matrix
{
}

Quaternion::Quaternion(const Vector3& axis, float angleDeg)
{
	Set(axis, angleDeg);
}

void Quaternion::Set(const Vector3& axis, float angleDeg)
{
	float halfSin;
	float angle = angleDeg * PI / 360.0f;

	halfSin = sinf(angle) / 2;

	x = axis.x * halfSin;
	y = axis.y * halfSin;
	z = axis.z * halfSin;
	w = cosf(angle / 2);
}

void Quaternion::Set(const Matrix4& m)
{
	float xc = m.matrix[0][0] - m.matrix[1][1] - m.matrix[2][2];
	float yc = m.matrix[1][1] - m.matrix[0][0] - m.matrix[2][2];
	float zc = m.matrix[2][2] - m.matrix[0][0] - m.matrix[1][1];
	float wc = m.matrix[0][0] + m.matrix[1][1] + m.matrix[2][2];

	int biggestIndex = 0;
	float biggestValue = wc;

	if(xc > biggestValue)
	{
		biggestValue = xc;
		biggestIndex = 1;
	}

	if(yc > biggestValue)
	{
		biggestValue = yc;
		biggestIndex = 2;
	}

	if(zc > biggestValue)
	{
		biggestValue = zc;
		biggestIndex = 3;
	}

	biggestValue = sqrtf(biggestValue + 1) / 2.0f;
	float mult = 0.25f / biggestValue;

	switch(biggestIndex)
	{
	case 0:
		w = biggestValue;
		x = (m.matrix[2][1] - m.matrix[1][2]) * mult;
		y = (m.matrix[0][2] - m.matrix[2][0]) * mult;
		z = (m.matrix[1][0] - m.matrix[0][1]) * mult;
		break;

	case 1:
		w = (m.matrix[2][1] - m.matrix[1][2]) * mult;
		x = biggestValue;
		y = (m.matrix[1][0] - m.matrix[0][1]) * mult;
		z = (m.matrix[0][2] - m.matrix[2][0]) * mult;
		break;

	case 2:
		w = (m.matrix[0][2] - m.matrix[2][0]) * mult;
		x = (m.matrix[1][0] - m.matrix[0][1]) * mult;
		y = biggestValue;
		z = (m.matrix[2][1] - m.matrix[1][2]) * mult;
		break;

	case 3:
		w = (m.matrix[1][0] - m.matrix[0][1]) * mult;
		x = (m.matrix[0][2] - m.matrix[2][0]) * mult;
		y = (m.matrix[2][1] - m.matrix[1][2]) * mult;
		z = biggestValue;
		break;
	}
}

Matrix4 Quaternion::GetRotationMatrix() const
{
	Matrix4 r;

	memset(r.matrix_a, 0, 16 * sizeof(float));

	r.matrix[0][0] = 1 - 2*y*y - 2*z*z;
	r.matrix[0][1] = 2*x*y - 2*w*z;
	r.matrix[0][2] = 2*x*z + 2*w*y;
	r.matrix[1][0] = 2*x*y + 2*w*z;
	r.matrix[1][1] = 1 - 2*x*x - 2*z*z;
	r.matrix[1][2] = 2*y*z - 2*w*x;
	r.matrix[2][0] = 2*x*z - 2*w*y;
	r.matrix[2][1] = 2*y*z + 2*w*x;
	r.matrix[2][2] = 1 - 2*x*x - 2*y*y;

	r.matrix[3][3] = 1.0f;

	return r;
}

void Quaternion::Normalize()
{
	float size = sqrtf( x*x + y*y + z*z + w*w);

	x /= size;
	y /= size;
	z /= size;
	w /= size;
}

Quaternion Quaternion::Conjugate(const Quaternion& q)
{
	Quaternion r;

	r.x = -q.x;
	r.y = -q.y;
	r.z = -q.z;
	r.w = q.w;

	return r;
}

Quaternion Quaternion::Inverse(const Quaternion& q)
{
	Quaternion r;

	r = Conjugate(q) / Dot(q, q);

	return r;
}

float Quaternion::Dot(const Quaternion& q1, const Quaternion& q2)
{
	return q1.x*q2.x + q1.y*q2.y + q1.z*q2.z + q1.w*q2.w;
}

Quaternion Quaternion::Cross(const Quaternion& q1, const Quaternion& q2)
{
	Quaternion r;

	r.x = q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y;
	r.y = q1.w * q2.y + q1.y * q2.w + q1.z * q2.x - q1.x * q2.z;
	r.z = q1.w * q2.z + q1.z * q2.w + q1.x * q2.y - q1.y * q2.x;
	r.w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;

	return r;
}

Quaternion Quaternion::Interpolate(const Quaternion& q1, const Quaternion& q2, const float& a)
{
	Quaternion r;

	float angle = acosf(Dot(q1, q2));

	r = (q1 * sinf((1.0f - a)*angle) + q2 * sinf(a * angle)) / sinf(angle);

	return r;
}

Quaternion Math::operator+ (const Quaternion& a, const Quaternion& b)
{
	Quaternion r;

	r.x = a.x + b.x;
	r.y = a.y + b.y;
	r.z = a.z + b.z;
	r.w = a.w + b.w;

	return r;
}

Quaternion Math::operator* (const Quaternion& a, const Quaternion& b)
{
	Quaternion r;

	r.x = a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y;
	r.y = a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z;
	r.z = a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x;
	r.w = a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z;

	return r;
}

Quaternion Math::operator* (const Quaternion& a, const float& b)
{
	Quaternion r;

	r.x = a.x * b;
	r.y = a.y * b;
	r.z = a.z * b;
	r.w = a.w * b;

	return r;
}

Quaternion Math::operator/ (const Quaternion& a, const float& s)
{
	Quaternion r;

	r.x = a.x / s;
	r.y = a.y / s;
	r.z = a.z / s;
	r.w = a.w / s;

	return r;
}


