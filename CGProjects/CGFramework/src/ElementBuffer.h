
#pragma once

#include "Buffer.h"

namespace CGFramework
{

class ElementBuffer : public Buffer
{

friend class Object;

public:
	ElementBuffer();

	bool Init(GLushort numIndices, const void* data, unsigned int dataSize, GLenum usageHint = GL_STATIC_DRAW);

	inline void Bind() const;
	inline void Unbind() const;

	void DrawElements(GLenum polyMode = GL_TRIANGLES) const;

private:
	GLushort m_NumIndices;
};

}