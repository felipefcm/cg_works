
#include "precompiled.h"

#include "Mesh.h"

using namespace CGFramework;

Mesh::Mesh() : m_MaterialIndex(0)
{
}

void Mesh::AddArrayAttrib(const GenericAttrib& attr)
{
	m_ArrayBuffer.AddAttrib(attr);
}