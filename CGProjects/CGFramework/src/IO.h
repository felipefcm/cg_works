
#pragma once

#include "StdMath.h"

namespace CGFramework
{

namespace IO
{

static std::ofstream ShaderLogStream("ShaderLog.txt", std::ofstream::out);

void Print(const char* str);
void RequestKeyToContinue();
void CheckGLError();
void PrintMatrix(Math::Matrix4 matrix);
void* ReadTgaImage(const char* filename, int* width, int* height);
short le_short(unsigned char *bytes);

}

}