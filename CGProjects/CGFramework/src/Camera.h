
#pragma once

#include "StdMath.h"

namespace CGFramework
{

using Math::Vector3;
using Math::Matrix4;

class Camera
{

public:
	Camera();
	Camera(const Vector3& eye, const Vector3& center, const Vector3& up = Vector3(0, 1.0f, 0));

	void SetEye(const Vector3&);
	void SetCenter(const Vector3&);
	void SetUp(const Vector3&);

	Matrix4 GetViewMatrix() const;

private:
	Vector3 m_Eye;
	Vector3 m_Center;
	Vector3 m_Up;

};

}