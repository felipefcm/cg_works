
#pragma once

#include "StdMath.h"
#include "Color.h"
#include "Object.h"
#include "Shader.h"

namespace CGFramework
{

using Math::Vector3;

class Light : public Object
{

public:

	enum Mode
	{
		DIRECTIONAL,
		POINT
	} m_Mode;

	Light(const ShaderProgram& shader, const Mode mode);

	bool Init();

	bool m_bDrawSphere;

	void SetLightUniforms();

protected:
	
};

class LightModel
{

public:
	LightModel();

	enum Model
	{
		Flat,
		Gouraud,
		Phong
	};

private:


};

class DirectLight : public LightModel
{
};



}
