
#include "precompiled.h"

#include "MatrixStack.h"

using namespace CGFramework;

MatrixStack::MatrixStack()
{
}

void MatrixStack::Push(const Matrix4& m)
{
	m_Stack.push(m);
}

Matrix4 MatrixStack::Pop()
{
	Matrix4 r = m_Stack.top();
	m_Stack.pop();

	return r;
}

Matrix4 MatrixStack::Peek() const
{
	if(m_Stack.size() > 0)
		return m_Stack.top();
	else
		return Matrix4::Identity();
}

void MatrixStack::Clear()
{
	while(m_Stack.size() > 0)
		m_Stack.pop();
}

void MatrixStack::Translate(const Vector3& vec)
{
	Matrix4 current, newOne;
	
	current = this->Peek();

	newOne = current * Matrix4::TranslateMatrix(vec.x, vec.y, vec.z);

	m_Stack.push(newOne);
}

void MatrixStack::Scale(const Vector3& vec)
{
	Matrix4 current, newOne;
	
	current = this->Peek();

	newOne = current * Matrix4::ScaleMatrix(vec.x, vec.y, vec.z);

	m_Stack.push(newOne);
}

void MatrixStack::Rotate(float angleDeg, const Vector3& axis)
{
	Matrix4 current, newOne;
	
	current = this->Peek();

	newOne = current * Matrix4::RotateMatrix(angleDeg, axis.x, axis.y, axis.z);

	m_Stack.push(newOne);
}

void MatrixStack::Multiply(const Matrix4& m)
{
	Matrix4 current, newOne;
	
	current = this->Peek();

	newOne = current * m;

	m_Stack.push(newOne);
}


