
#pragma once

namespace CGFramework
{

namespace Math
{

struct Matrix4
{
	union
	{
		struct
		{
			float 
				m00, m01, m02, m03,
				m10, m11, m12, m13,
				m20, m21, m22, m23,
				m30, m31, m32, m33 ;
		};

		float matrix[4][4];

		float matrix_a[16];
	};

	Matrix4();
	Matrix4(const Matrix4&);

	//for wrapper uses
	static glm::mat4 GlmMat(const Matrix4&);
	static Matrix4 FwMat(const glm::mat4&);
	static Matrix4 FwMat(const aiMatrix4x4&);

	static Matrix4 Identity();
	static Matrix4 Transpose(const Matrix4& m);
	static Matrix4 Invert(const Matrix4& m);

	//Projection
	static Matrix4 StdPerspectiveMatrix(float angleDeg, float aspect, float zNear, float zFar);
	static Matrix4 ObliquePerspectiveMatrix();
	static Matrix4 OrthographicProjectionMatrix(float left, float right, float top, float bottom, float znear, float zfar);
	
	//Transformations
	static Matrix4 TranslateMatrix(float tx, float ty, float tz);
	static Matrix4 ScaleMatrix(float fx, float fy, float fz);
	static Matrix4 RotateMatrix(float angleDeg, float x, float y, float z);

	Matrix4 operator+ (const Matrix4& b);
	Matrix4 operator- (const Matrix4& b);
	Matrix4 operator* (const Matrix4& b);
	Matrix4 operator* (const float& a);
};

}

}