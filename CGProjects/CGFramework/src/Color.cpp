
#include "precompiled.h"

#include "Color.h"

using namespace CGFramework;

Color3f::Color3f() : r(0), g(0), b(0)
{
}

Color3f::Color3f(float r, float g, float b) : r(r), g(g), b(b)
{
}


Color3i::Color3i() : r(0), g(0), b(0)
{
}

Color3i::Color3i(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b)
{
}


Color4f::Color4f() : r(0), g(0), b(0), a(0)
{
}

Color4f::Color4f(float r, float g, float b, float a) : r(r), g(g), b(b), a(a)
{
}


Color4i::Color4i() : r(0), g(0), b(0), a(0)
{
}

Color4i::Color4i(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : r(r), g(g), b(b), a(a)
{
}
