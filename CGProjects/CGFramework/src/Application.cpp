
#include "precompiled.h"

#include "Application.h"
#include "IO.h"

namespace CGFramework
{

bool Application::m_IsRunning = false;
Application* Application::m_pApp = NULL;

Application::Application() : m_IsInit(false)
{
}

bool Application::Init(std::string windowTitle, GLuint width, GLuint height)
{
	IO::Print("Application initializing...");

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);

	glutInitContextVersion(3, 0);

	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 10);
	glutCreateWindow(windowTitle.c_str());

	IO::Print("	freeglut window created");

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);

	glutDisplayFunc(_Display);
	glutKeyboardFunc(_Keyboard);
	glutReshapeFunc(_Reshape);
	glutMouseFunc(_Mouse);
	glutCloseFunc(_Close);

	glewExperimental = GL_TRUE;
	if(glewInit() != GLEW_OK)
	{
		IO::Print("	GLEW failed to init");
		return false;
	}
	else
		IO::Print("	GLEW initialized");

	//consume INVALID_ENUM created by glewInit (glew 1.9.0 bug)
	glGetError();

	m_IsInit = true;

	IO::CheckGLError();

	IO::Print("Application initialized");

	return true;
}

int Application::Run()
{
	if(!m_IsInit)
	{
		//throw std::exception("Not init!");
		return -1;
	}

	m_pApp = this;

	m_IsRunning = true;

	IO::Print("Application is running...");

	while(m_IsRunning)
		glutMainLoopEvent();

	VCleanup();

	return 0;
}

//statics
void Application::_Display()
{
	m_pApp->VUpdate();

	m_pApp->VRender();

	glutPostRedisplay();
}

void Application::_Keyboard(unsigned char key, int x, int y)
{
	m_pApp->VKeyboardEvent(key, x, y);
}

void Application::_Reshape(int w, int h)
{
	IO::Print("Setting/Resizing viewport...");
	m_pApp->VResize(w, h);
}

void Application::_Mouse(int button, int state, int x, int y)
{
	m_pApp->VMouseEvent(button, state, x, y);
}

void Application::_Close()
{
	m_IsRunning = false;
}

void Application::VResize(int w, int h)
{
	//virtual
}

void Application::VKeyboardEvent(unsigned char key, int x, int y)
{
	//virtual
}

void Application::VMouseEvent(int button, int state, int x, int y)
{
	//virtual
}

}
