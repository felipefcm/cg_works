
#pragma once

namespace CGFramework
{

class VertexArrayObject
{

public:
	VertexArrayObject();

	bool Init();

	inline void Bind();
	inline void Unbind();

private:
	GLuint m_VAO;

};

}
