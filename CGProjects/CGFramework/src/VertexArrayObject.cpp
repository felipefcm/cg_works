
#include "precompiled.h"

#include "VertexArrayObject.h"

using namespace CGFramework;

VertexArrayObject::VertexArrayObject() : m_VAO(0)
{
}

bool VertexArrayObject::Init()
{
	glGenVertexArrays(1, &m_VAO);

	return true;
}

void VertexArrayObject::Bind()
{
	glBindVertexArray(m_VAO);
}

void VertexArrayObject::Unbind()
{
	glBindVertexArray(0);
}
