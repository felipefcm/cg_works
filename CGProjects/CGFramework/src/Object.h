
#pragma once

#include "StdMath.h"
#include "Shader.h"
#include "Mesh.h"
#include "IO.h"
#include "TextureBuffer.h"

namespace CGFramework
{

using namespace Math;

class Object
{

public:
	Object(const ShaderProgram& program);

	bool LoadModel(const std::string& filename);

	void AddMesh(const Mesh& m);

	void Render() const;

	const ShaderProgram* GetShader() const;

private:
	std::vector<Mesh> m_Meshes;
	std::vector<TextureBuffer> m_Textures;

	const ShaderProgram& m_ShaderProgram;

};

}
