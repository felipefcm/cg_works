
#include "precompiled.h"

#include "Shader.h"
#include "IO.h"

namespace CGFramework
{

Shader::Shader()
{
}

bool Shader::Create(Type shaderType, const std::string& shaderSrc)
{
	m_Shader = glCreateShader(shaderType);

	const char* src = shaderSrc.c_str();

	glShaderSource(m_Shader, 1, &src, NULL);

	glCompileShader(m_Shader);

	GLint status;
	glGetShaderiv(m_Shader, GL_COMPILE_STATUS, &status);

	//report errors and warnings to stream
	GLint logLength = 0;
	glGetShaderiv(m_Shader, GL_INFO_LOG_LENGTH, &logLength);

	if(logLength > 1)
	{
		char* buffer = new char[logLength];

		glGetShaderInfoLog(m_Shader, logLength, NULL, buffer);

		buffer[logLength-1] = '\0';

		IO::ShaderLogStream << m_FileName << ' ' << buffer;

		delete[] buffer;
	}

	return (status != GL_FALSE);
}

bool Shader::CreateFromFile(Type shaderType, const std::string& filename)
{
	m_FileName = filename;
	std::ifstream stream(filename);

	std::string src((std::istreambuf_iterator<char>(stream)), (std::istreambuf_iterator<char>()));

	bool ret = Create(shaderType, src);

	stream.close();

	return ret;
}

void Shader::Delete()
{
	glDeleteShader(m_Shader);
}

//ShaderProgram

ShaderProgram::ShaderProgram()
{
}

bool ShaderProgram::Init()
{
	m_Program = glCreateProgram();

	return true;
}

void ShaderProgram::AttachShader(const Shader& shader)
{
	glAttachShader(m_Program, shader.m_Shader);
}

bool ShaderProgram::Create()
{
	glLinkProgram(m_Program);

	GLint status;
	glGetProgramiv(m_Program, GL_LINK_STATUS, &status);

	//report errors and warnings to stream
	GLint logLength = 0;
	glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &logLength);

	if(logLength > 1)
	{
		char* buffer = new char[logLength];

		glGetProgramInfoLog(m_Program, logLength, NULL, buffer);

		buffer[logLength-1] = '\0';

		IO::ShaderLogStream << buffer;

		delete[] buffer;
	}

	return (status != GL_FALSE);
}

void ShaderProgram::Use() const
{
	glUseProgram(m_Program);
}

void ShaderProgram::Release() const
{
	glUseProgram(0);
}

GLuint const ShaderProgram::GetUniformLocation(const std::string& name) const
{
	return glGetUniformLocation(m_Program, name.c_str());
}

const std::string DefaultShaders::VertexDefault =
	"#version 330\n"
	"layout(location = 0) in vec4 position;\n"
	"layout(location = 1) in vec4 outputColor;\n"
	"smooth out vec4 color;\n"
	"void main()\n"
	"{\n"
	"	gl_Position = position;\n"
	"	color = outputColor;\n"
	"}\n";

const std::string DefaultShaders::FragmentDefault =
	"#version 330\n"
	"smooth in vec4 color;\n"
	"out vec4 outputColor;\n"
	"void main()\n"
	"{\n"
	"   outputColor = color;\n"
	"}\n";

}

