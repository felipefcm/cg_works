
#pragma once

namespace CGFramework
{

enum ValueType
{
	IntType = GL_INT,
	FloatType = GL_FLOAT,
	DoubleType = GL_DOUBLE
};

struct GenericAttrib
{
	GLuint Index;
	GLushort Size;
	ValueType Value;
	GLuint Stride;
	void* Offset;
};

class Buffer
{

public:
	Buffer();

	virtual inline void Bind() const = 0;
	virtual inline void Unbind() const = 0;

protected:
	GLuint m_BufferHandle;

};

}