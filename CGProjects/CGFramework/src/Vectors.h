
#pragma once

namespace CGFramework
{

namespace Math
{

struct Vector3
{
	union
	{
		struct
		{
			float x, y, z;
		};

		float coords[3];
	};

	Vector3() : x(0), y(0), z(0) {}
	Vector3(float x, float y, float z = 0) : x(x), y(y), z(z) {}

	void Normalize();

	static float Dot(const Vector3&, const Vector3&);
	static Vector3 Cross(const Vector3&, const Vector3&);

	static Vector3 Interpolate(const Vector3& v1, const Vector3& v2, float alpha);
};

struct Vector4
{
	union
	{
		struct
		{
			float x, y, z, w;
		};

		float coords[4];
	};

	Vector4() : x(0), y(0), z(0), w(1.0f) {}
	Vector4(float x, float y, float z = 0, float w = 1.0f) : x(x), y(y), z(z), w(w) {}

	void Normalize();
};

Vector3 operator+ (const Vector3&, const Vector3&);
Vector3 operator- (const Vector3&, const Vector3&);
Vector3 operator* (const Vector3&, const float&);

Vector4 operator+ (const Vector4&, const Vector4&);
Vector4 operator- (const Vector4&, const Vector4&);
Vector4 operator* (const Vector4&, const float&);

}

}