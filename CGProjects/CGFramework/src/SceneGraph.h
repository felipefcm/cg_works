
#pragma once

#include "MatrixStack.h"
#include "Object.h"

namespace CGFramework
{

using namespace Math;

class SceneNode
{

public:
	SceneNode();

	friend class SceneGraph;

	virtual void VRender(MatrixStack& stack) const = 0;

	void SetTransform(const Matrix4& mat);
	Matrix4 GetTransform() const;

protected:
	Matrix4 m_Transform;

};

class GroupNode : public SceneNode
{

public:
	GroupNode();

	void AddChild(SceneNode& kid);

	void VRender(MatrixStack& stack) const;

private:
	std::vector<SceneNode*> m_Childs;

};

class LeafNode : public SceneNode
{

public:
	LeafNode();
	LeafNode(Object& obj);

	void SetObject(Object& obj);

	void VRender(MatrixStack& stack) const;

private:
	Object* m_pObject;

};

class SceneGraph
{

public:
	SceneGraph();

	void Render();

	void SetRoot(SceneNode* pNode);

	std::vector<SceneNode*> m_Nodes;

private:
	MatrixStack m_Stack;
	
	SceneNode* m_Root;

	void Render(const SceneNode& root);

};

}
