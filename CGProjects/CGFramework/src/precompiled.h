
#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <exception>
#include <vector>
#include <stack>
#include <cassert>
#include <memory>
#include <cstdint>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
