
#include "precompiled.h"

#include "Object.h"

using namespace CGFramework;

Object::Object(const ShaderProgram& program) : m_ShaderProgram(program)
{
}

bool Object::LoadModel(const std::string& filename)
{
	Assimp::Importer aiImporter;
	const aiScene* scene = NULL;

	scene = aiImporter.ReadFile(filename, aiProcessPreset_TargetRealtime_MaxQuality);

	if(scene == NULL)
	{
		std::string e("Error loading model: ");
		e.append(filename);

		IO::Print(e.c_str());
		IO::Print(aiImporter.GetErrorString());

		return false;
	}

	for(int i=0; i< scene->mNumMeshes; ++i)
	{
		std::vector<Vector3> vertices;
		std::vector<GLushort> indices;

		Mesh mesh;
		aiMesh* pMesh = scene->mMeshes[i];

		bool hasTexture = pMesh->HasTextureCoords(0);

		mesh.m_MaterialIndex = pMesh->mMaterialIndex;

		//Load vertices and normals interleaved
		for(int v=0; v< pMesh->mNumVertices; ++v)
		{
			Vector3 pos(
				pMesh->mVertices[v].x,
				pMesh->mVertices[v].y,
				pMesh->mVertices[v].z
				);

			Vector3 normal(
				pMesh->mNormals[v].x,
				pMesh->mNormals[v].y,
				pMesh->mNormals[v].z
				);

			Vector3 texCoord(0, 0, 0);

			if(hasTexture)
				texCoord = Vector3(
					pMesh->mTextureCoords[0][v].x,
					pMesh->mTextureCoords[0][v].y
					);

			vertices.push_back(pos);
			vertices.push_back(normal);

			if(hasTexture)
				vertices.push_back(texCoord);
		}

		GLushort numIndices = 0;

		for(int j=0; j< pMesh->mNumFaces; ++j)
		{
			aiFace face = pMesh->mFaces[j];

			numIndices += face.mNumIndices;

			for(int f=0; f< face.mNumIndices; ++f)
				indices.push_back(face.mIndices[f]);
		}

		//finished loading mesh, creating buffer objects
		if( !mesh.m_ArrayBuffer.Init(&vertices[0], sizeof(Vector3) * vertices.size()) ||
			!mesh.m_ElementBuffer.Init(numIndices, &indices[0], sizeof(GLushort) * indices.size()) )
		{
			IO::Print("Error creating buffer objects for model");
			return false;
		}

		//Setting attributes
		GenericAttrib position, normal, texCoord;

		position.Index = 0;
		position.Size = 3;
		position.Value = FloatType;
		position.Offset = (void*) 0;

		if(hasTexture)
			position.Stride = 9 * sizeof(float);
		else
			position.Stride = 6 * sizeof(float);

		normal.Index = 1;
		normal.Size = 3;
		normal.Value = FloatType;
		normal.Offset = (void*) (3 * sizeof(float));

		if(hasTexture)
			normal.Stride = 9 * sizeof(float);
		else
			normal.Stride = 6 * sizeof(float);

		texCoord.Index = 2;
		texCoord.Size = 3;
		texCoord.Value = FloatType;
		texCoord.Offset = (void*) (6 * sizeof(float));
		texCoord.Stride = 9 * sizeof(float);

		mesh.AddArrayAttrib(position);
		mesh.AddArrayAttrib(normal); 

		if(hasTexture)
			mesh.AddArrayAttrib(texCoord);

		m_Meshes.push_back(mesh);

		vertices.clear();
		indices.clear();
	}

	//loading materials
	for(int i=0; i< scene->mNumMaterials; ++i)
	{
		const aiMaterial* pMat = scene->mMaterials[i];

		for(int j=0; j< pMat->GetTextureCount(aiTextureType_DIFFUSE); ++j)
		{
			aiString path;

			pMat->GetTexture(aiTextureType_DIFFUSE, 0, &path);

			std::string file;
			file = "models/";
			file += path.C_Str();

			TextureBuffer tb(m_ShaderProgram.GetUniformLocation("textureSampler"));
			tb.Init(TexType::Texture2D, file.c_str());

			m_Textures.push_back(tb);
		}
	}

	return true;
}

const ShaderProgram* Object::GetShader() const
{
	return &m_ShaderProgram;
}

void Object::Render() const
{
	const GenericAttrib* pAttrib;

	m_ShaderProgram.Use();

	for(int i=0; i< m_Meshes.size(); ++i)
	{
		m_Meshes[i].m_ArrayBuffer.Bind();

		for(int j=0; j< m_Meshes[i].m_ArrayBuffer.m_Attribs.size(); ++j)
		{
			pAttrib = & m_Meshes[i].m_ArrayBuffer.m_Attribs[j];
			glEnableVertexAttribArray(pAttrib->Index);
			glVertexAttribPointer(pAttrib->Index, pAttrib->Size, pAttrib->Value, GL_FALSE, pAttrib->Stride, pAttrib->Offset);
		}

		if(m_Textures.size() > 0)
		{
			int samplerUniform = m_ShaderProgram.GetUniformLocation("textureSampler");

			m_Textures[m_Meshes[i].m_MaterialIndex-1].Bind();

			glUniform1i(samplerUniform, m_Textures[m_Meshes[i].m_MaterialIndex-1].GetTexUnit());
		}

		//already binds the element buffer
		m_Meshes[i].m_ElementBuffer.DrawElements(GL_TRIANGLES);		

		if(m_Textures.size() > 0)
			m_Textures[m_Meshes[i].m_MaterialIndex-1].Unbind();

		for(int j=0; j< m_Meshes[i].m_ArrayBuffer.m_Attribs.size(); ++j)
		{
			pAttrib = & m_Meshes[i].m_ArrayBuffer.m_Attribs[j];
			glDisableVertexAttribArray(pAttrib->Index);
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	m_ShaderProgram.Release();
}

