
#include "precompiled.h"

#include "IO.h"

using namespace CGFramework;
using namespace IO;

void IO::Print(const char* str)
{
	std::cout << str << std::endl;
}

void IO::CheckGLError()
{
	GLenum error = glGetError();

	if(error != GL_NO_ERROR)
	{
		std::string errorStr = "Error in OGL: ";

		switch(error)
		{
			case GL_INVALID_ENUM:
				errorStr.append("INVALID_ENUM");
			break;

			case GL_INVALID_VALUE:
				errorStr.append("INVALID_VALUE");
			break;

			case GL_INVALID_OPERATION:
				errorStr.append("INVALID_OPERATION");
			break;

			case GL_INVALID_FRAMEBUFFER_OPERATION:
				errorStr.append("INVALID_FRAMEBUFFER_OPERATION");
			break;

			case GL_OUT_OF_MEMORY:
				errorStr.append("OUT_OF_MEMORY");
			break;

			case GL_STACK_UNDERFLOW:
				errorStr.append("STACK_UNDERFLOW");
			break;

			case GL_STACK_OVERFLOW:
				errorStr.append("STACK_OVERFLOW");
			break;
		}

		Print(errorStr.c_str());
	}	
}

void IO::PrintMatrix(Math::Matrix4 matrix)
{
	for(int i=0; i< 4; ++i)
	{
		for(int j=0; j< 4; ++j)
			std::cout << matrix.matrix[i][j] << ",";

		std::cout<<std::endl;
	}
}

void IO::RequestKeyToContinue()
{
	Print("Press any key to continue...");
	std::getchar();
}

short IO::le_short(unsigned char *bytes)
{
    return bytes[0] | ((char)bytes[1] << 8);
}

void* IO::ReadTgaImage(const char* filename, int* width, int* height)
{
    struct tga_header {
       char id_length;
       char color_map_type;
       char data_type_code;
       unsigned char color_map_origin[2];
       unsigned char color_map_length[2];
       char color_map_depth;
       unsigned char x_origin[2];
       unsigned char y_origin[2];
       unsigned char width[2];
       unsigned char height[2];
       char bits_per_pixel;
       char image_descriptor;
    } header;

    int i, color_map_size, pixels_size;
    FILE *f;
    size_t read;
    void *pixels;

    fopen_s(&f, filename, "rb");

    if (!f) {
		IO::Print("Error reading image file");
        return NULL;
    }

    read = fread(&header, 1, sizeof(header), f);

    if (read != sizeof(header)) {
        IO::Print("Incomplete TGA header found");
        fclose(f);
        return NULL;
    }
    if (header.data_type_code != 2) {
        IO::Print("Invalid TGA format");
        fclose(f);
        return NULL;
    }
    if (header.bits_per_pixel != 24) {
        IO::Print("Invalid 24-bit TGA format");
        fclose(f);
        return NULL;
    }

    for (i = 0; i < header.id_length; ++i)
        if (getc(f) == EOF) {
            IO::Print("Invalid TGA id string");
            fclose(f);
            return NULL;
        }

    color_map_size = le_short(header.color_map_length) * (header.color_map_depth/8);
    for (i = 0; i < color_map_size; ++i)
        if (getc(f) == EOF) {
            IO::Print("Invalid TGA color map");
            fclose(f);
            return NULL;
        }

    *width = le_short(header.width); *height = le_short(header.height);
    pixels_size = *width * *height * (header.bits_per_pixel/8);
    pixels = new char[pixels_size];

    read = fread(pixels, 1, pixels_size, f);
    fclose(f);

    if (read != pixels_size) {
        IO::Print("Invalid TGA image");
        delete[] pixels;
        return NULL;
    }

    return pixels;
}