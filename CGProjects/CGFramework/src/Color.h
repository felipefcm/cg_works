
#pragma once

namespace CGFramework
{

struct Color3f
{
	Color3f();
	Color3f(float r, float g, float b);

	union
	{
		struct
		{
			float r, g, b;
		};

		float c[3];
	};
};

struct Color3i
{
	Color3i();
	Color3i(uint8_t r, uint8_t g, uint8_t b);

	union
	{
		struct
		{
			uint8_t r, g, b;
		};

		uint8_t c[3];
	};
};

struct Color4f
{
	Color4f();
	Color4f(float r, float g, float b, float a);

	union
	{
		struct
		{
			float r, g, b, a;
		};

		float c[4];
	};
};

struct Color4i
{
	Color4i();
	Color4i(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

	union
	{
		struct
		{
			uint8_t r, g, b, a;
		};

		uint8_t c[4];
	};
};

}