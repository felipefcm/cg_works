
#include "precompiled.h"

#include "ElementBuffer.h"

using namespace CGFramework;

ElementBuffer::ElementBuffer() : m_NumIndices(0)
{
}

bool ElementBuffer::Init(GLushort numIndices, const void* data, unsigned int dataSize, GLenum usageHint)
{
	if(data == NULL || dataSize == 0 || numIndices <= 0)
		return false;

	m_NumIndices = numIndices;

	glGenBuffers(1, &m_BufferHandle);

	Bind();

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, dataSize, data, usageHint);
	
	Unbind();

	return true;
}

void ElementBuffer::DrawElements(GLenum polyMode) const
{
	Bind();
	glDrawElements(polyMode, m_NumIndices, GL_UNSIGNED_SHORT, 0);
	Unbind();
}

void ElementBuffer::Bind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferHandle);
}

void ElementBuffer::Unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}