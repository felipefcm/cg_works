
#pragma once

#include "Buffer.h"

namespace CGFramework
{

class ArrayBuffer : public Buffer
{

friend class Object;

public:
	ArrayBuffer();

	bool Init(const void* data, unsigned int size, GLenum usageHint = GL_STATIC_DRAW);

	inline void Bind() const;
	inline void Unbind() const;

	void AddAttrib(const GenericAttrib& attrib);

private:
	std::vector<GenericAttrib> m_Attribs;

};

}
