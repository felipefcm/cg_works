
#include "precompiled.h"

#include "Camera.h"

using namespace CGFramework;

Camera::Camera()
{
}

Camera::Camera(const Vector3& eye, const Vector3& center, const Vector3& up) : m_Eye(eye), m_Center(center), m_Up(up)
{
}

void Camera::SetEye(const Vector3& eye)
{
	m_Eye = eye;
}

void Camera::SetCenter(const Vector3& center)
{
	m_Center = center;
}

void Camera::SetUp(const Vector3& up)
{
	m_Up = up;
}

Matrix4 Camera::GetViewMatrix() const
{
	Matrix4 r;

	Vector3 f = m_Center - m_Eye;
	f.Normalize();

	Vector3 u = m_Up;
	u.Normalize();

	Vector3 s = Vector3::Cross(f, u);
	s.Normalize();

	u = Vector3::Cross(s, f);

	r.matrix[0][0] = s.x;
	r.matrix[0][1] = s.y;
	r.matrix[0][2] = s.z;
	r.matrix[1][0] = u.x;
	r.matrix[1][1] = u.y;
	r.matrix[1][2] = u.z;
	r.matrix[2][0] = -f.x;
	r.matrix[2][1] = -f.y;
	r.matrix[2][2] = -f.z;
	r.matrix[0][3] = -Vector3::Dot(s, m_Eye);
	r.matrix[1][3] = -Vector3::Dot(u, m_Eye);
	r.matrix[2][3] = -Vector3::Dot(f, m_Eye);

	r.matrix[3][0] = 0;
	r.matrix[3][1] = 0;
	r.matrix[3][2] = 0;
	r.matrix[3][3] = 1.0f;

	return r;
}

