
#include "precompiled.h"

#include "Matrix.h"
#include "StdMath.h"

namespace CGFramework
{

using Math::Matrix4;

Matrix4::Matrix4()
{
}

Matrix4::Matrix4(const Matrix4& m)
{
	memcpy(matrix_a, m.matrix_a, 16 * sizeof(float));
}

Matrix4 Matrix4::operator+ (const Matrix4& b)
{
	Matrix4 r;

	for(int i=0; i< 16; ++i)
		r.matrix_a[i] = matrix_a[i] + b.matrix_a[i];

	return r;
}

Matrix4 Matrix4::operator- (const Matrix4& b)
{
	Matrix4 r;

	for(int i=0; i< 16; ++i)
		r.matrix_a[i] = matrix_a[i] - b.matrix_a[i];

	return r;
}

Matrix4 Matrix4::operator* (const Matrix4& b)
{
	Matrix4 r;
	int i, j, k;

	for(i=0; i< 4; ++i)
	{
		for(j=0; j< 4; ++j)
		{
			r.matrix[i][j] = 0;

			for(k=0; k< 4; ++k)
				r.matrix[i][j] += matrix[i][k] * b.matrix[k][j];
		}
	}

	return r;
}

Matrix4 Matrix4::operator* (const float& a)
{
	Matrix4 r;

	for(int i=0; i< 16; ++i)
		r.matrix_a[i] = matrix_a[i] * a;

	return r;
}

glm::mat4 Matrix4::GlmMat(const Matrix4& m)
{
	glm::mat4 r;

	r[0][0] = m.matrix[0][0];
	r[0][1] = m.matrix[1][0];
	r[0][2] = m.matrix[2][0];
	r[0][3] = m.matrix[3][0];
	r[1][0] = m.matrix[0][1];
	r[1][1] = m.matrix[1][1];
	r[1][2] = m.matrix[2][1];
	r[1][3] = m.matrix[3][1];
	r[2][0] = m.matrix[0][2];
	r[2][1] = m.matrix[1][2];
	r[2][2] = m.matrix[2][2];
	r[2][3] = m.matrix[3][2];
	r[3][0] = m.matrix[0][3];
	r[3][1] = m.matrix[1][3];
	r[3][2] = m.matrix[2][3];
	r[3][3] = m.matrix[3][3];

	return r;
}

Matrix4 Matrix4::FwMat(const glm::mat4& m)
{
	Matrix4 r;

	r.matrix[0][0] = m[0][0];
	r.matrix[0][1] = m[1][0];
	r.matrix[0][2] = m[2][0];
	r.matrix[0][3] = m[3][0];
	r.matrix[1][0] = m[0][1];
	r.matrix[1][1] = m[1][1];
	r.matrix[1][2] = m[2][1];
	r.matrix[1][3] = m[3][1];
	r.matrix[2][0] = m[0][2];
	r.matrix[2][1] = m[1][2];
	r.matrix[2][2] = m[2][2];
	r.matrix[2][3] = m[3][2];
	r.matrix[3][0] = m[0][3];
	r.matrix[3][1] = m[1][3];
	r.matrix[3][2] = m[2][3];
	r.matrix[3][3] = m[3][3];

	return r;
}

Matrix4 Matrix4::FwMat(const aiMatrix4x4& m)
{
	Matrix4 r;

	r.matrix[0][0] = m[0][0];
	r.matrix[0][1] = m[1][0];
	r.matrix[0][2] = m[2][0];
	r.matrix[0][3] = m[3][0];
	r.matrix[1][0] = m[0][1];
	r.matrix[1][1] = m[1][1];
	r.matrix[1][2] = m[2][1];
	r.matrix[1][3] = m[3][1];
	r.matrix[2][0] = m[0][2];
	r.matrix[2][1] = m[1][2];
	r.matrix[2][2] = m[2][2];
	r.matrix[2][3] = m[3][2];
	r.matrix[3][0] = m[0][3];
	r.matrix[3][1] = m[1][3];
	r.matrix[3][2] = m[2][3];
	r.matrix[3][3] = m[3][3];

	return r;
}

Matrix4 Matrix4::Identity()
{
	Matrix4 r;

	float id[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	memcpy(r.matrix_a, id, 16 * sizeof(float));
		
	return r;
}

Matrix4 Matrix4::Transpose(const Matrix4& m)
{
	Matrix4 r;

	r.matrix[0][0] = m.matrix[0][0];
	r.matrix[0][1] = m.matrix[1][0];
	r.matrix[0][2] = m.matrix[2][0];
	r.matrix[0][3] = m.matrix[3][0];
	r.matrix[1][0] = m.matrix[0][1];
	r.matrix[1][1] = m.matrix[1][1];
	r.matrix[1][2] = m.matrix[2][1];
	r.matrix[1][3] = m.matrix[3][1];
	r.matrix[2][0] = m.matrix[0][2];
	r.matrix[2][1] = m.matrix[1][2];
	r.matrix[2][2] = m.matrix[2][2];
	r.matrix[2][3] = m.matrix[3][2];
	r.matrix[3][0] = m.matrix[0][3];
	r.matrix[3][1] = m.matrix[1][3];
	r.matrix[3][2] = m.matrix[2][3];
	r.matrix[3][3] = m.matrix[3][3];

	return r;
}

Matrix4 Matrix4::Invert(const Matrix4& m)
{
	Matrix4 r;
	glm::mat4 glmMat;
	
	glmMat = glm::inverse(GlmMat(m));

	r = FwMat(glmMat);

	return r;
}

Matrix4 Matrix4::StdPerspectiveMatrix(float angleDeg, float aspect, float zNear, float zFar)
{
	Matrix4 r;

	memset(r.matrix_a, 0, 16 * sizeof(float));

	float f = 1 / tanf(angleDeg/2);

	r.matrix[0][0] = f / aspect;

	r.matrix[1][1] = f;

	r.matrix[2][2] = (zFar + zNear) / (zNear - zFar);
	r.matrix[2][3] = 2 * zFar * zNear / (zNear - zFar);

	r.matrix[3][2] = -1.0f;

	return r;
}

Matrix4 Matrix4::OrthographicProjectionMatrix(float left, float right, float top, float bottom, float znear, float zfar)
{
	Matrix4 r;

	memset(r.matrix_a, 0, sizeof(float) * 16);

	r.matrix[0][0] = 2 / (right - left);
	r.matrix[1][1] = -((right + left) / (right - left));

	r.matrix_a[5] = 2 / (top - bottom);
	r.matrix_a[7] = -((top + bottom) / (top - bottom));
	
	r.matrix_a[10] = -2 / (zfar - znear);
	r.matrix_a[11] = -((zfar + znear) / (zfar - znear));

	r.matrix_a[15] = 1.0f;

	return r;
}

Matrix4 Matrix4::TranslateMatrix(float tx, float ty, float tz)
{
	Matrix4 r;
	
	memset(r.matrix_a, 0, 16 * sizeof(float));

	r.matrix[0][0] = 1.0f;
	r.matrix[1][1] = 1.0f;
	r.matrix[2][2] = 1.0f;
	r.matrix[3][3] = 1.0f;

	r.matrix[0][3] = tx;
	r.matrix[1][3] = ty;
	r.matrix[2][3] = tz;

	return r;
}

Matrix4 Matrix4::ScaleMatrix(float fx, float fy, float fz)
{
	Matrix4 r;
	
	memset(r.matrix_a, 0, 16 * sizeof(float));

	r.matrix[0][0] = fx;
	r.matrix[1][1] = fy;
	r.matrix[2][2] = fz;
	r.matrix[3][3] = 1.0f;

	return r;
}

Matrix4 Matrix4::RotateMatrix(float angleDeg, float x, float y, float z)
{
	Matrix4 r;		

	memset(r.matrix_a, 0, 16 * sizeof(float));

	float rad = angleDeg * PI / 180.0f;

	float c = cos(rad);
	float s = sin(rad);
	float iC = 1 - c;

	r.matrix_a[0] = x*x*iC + c;
	r.matrix_a[1] = x*y*iC - z*s;
	r.matrix_a[2] = x*z*iC + y*s;

	r.matrix_a[4] = x*y*iC + z*s;
	r.matrix_a[5] = y*y*iC + c;
	r.matrix_a[6] = y*z*iC - x*s;

	r.matrix_a[8] = x*z*iC - y*s;
	r.matrix_a[9] = y*z*iC + x*s;
	r.matrix_a[10] = z*z*iC + c;

	r.matrix_a[15] = 1.0f;

	return r;
}

}