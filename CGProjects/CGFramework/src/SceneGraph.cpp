
#include "precompiled.h"

#include "SceneGraph.h"

using namespace CGFramework;

//Node
SceneNode::SceneNode() : m_Transform(Matrix4::Identity())
{
}

void SceneNode::SetTransform(const Matrix4& mat)
{
	m_Transform = mat;
}

Matrix4 SceneNode::GetTransform() const
{
	return m_Transform;
}


//Group
GroupNode::GroupNode()
{
}

void GroupNode::AddChild(SceneNode& kid)
{
	m_Childs.push_back(&kid);
}

void GroupNode::VRender(MatrixStack& stack) const
{
	stack.Push(stack.Peek() * m_Transform);

	for(int i=0; i< m_Childs.size(); ++i)
		m_Childs[i]->VRender(stack);

	stack.Pop();
}

//Leaf
LeafNode::LeafNode() : m_pObject(NULL)
{
}

LeafNode::LeafNode(Object& obj) : m_pObject(&obj)
{
}

void LeafNode::SetObject(Object& obj)
{
	m_pObject = &obj;
}

void LeafNode::VRender(MatrixStack& stack) const
{
	stack.Push(stack.Peek() * m_Transform);

	int matrixUniform = m_pObject->GetShader()->GetUniformLocation("objectToWorldMatrix");

	m_pObject->GetShader()->Use();
	glUniformMatrix4fv(matrixUniform, 1, GL_TRUE, stack.Peek().matrix_a);
	m_pObject->GetShader()->Release();

	m_pObject->Render();

	stack.Pop();
}

//Graph
SceneGraph::SceneGraph() : m_Root(NULL)
{
}

void SceneGraph::SetRoot(SceneNode* pNode)
{
	m_Root = pNode;
}

void SceneGraph::Render()
{
	if(m_Root != NULL)
		Render(*m_Root);
	else
		IO::Print("ERROR: SceneGraph root node is null");
}

void SceneGraph::Render(const SceneNode& root)
{
	m_Stack.Clear();

	root.VRender(m_Stack);
}
