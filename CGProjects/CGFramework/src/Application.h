
#pragma once

namespace CGFramework
{

class Application
{

public:

	Application();
	Application(const Application&){};

	bool Init(std::string windowTitle, GLuint width = 800, GLuint height = 600);

	int Run();
	
	virtual void VUpdate() = 0;

	virtual void VRender() = 0;

	virtual void VCleanup() = 0;

	virtual void VResize(int w, int h);

	virtual void VKeyboardEvent(unsigned char key, int x, int y);
	virtual void VMouseEvent(int button, int state, int x, int y);

private:

	static bool m_IsRunning;
	bool m_IsInit;
	static Application* m_pApp;

	//Callback for freeglut
	static void _Display();
	static void _Keyboard(unsigned char key, int x, int y);
	static void _Reshape(int w, int h);
	static void _Mouse(int button, int state, int x, int y);
	static void _Close();

};

}
