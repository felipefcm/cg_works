
#pragma once

namespace CGFramework
{

class Shader
{

public:

	enum Type
	{
		VERTEX = GL_VERTEX_SHADER,
		FRAGMENT = GL_FRAGMENT_SHADER,
		GEOMETRY = GL_GEOMETRY_SHADER
	};

	Shader();

	bool Create(Type shaderType, const std::string& shaderSrc);
	bool CreateFromFile(Type shaderType, const std::string& filename);

	void Delete();

	friend class ShaderProgram;

private:
	GLuint m_Shader;
	std::string m_FileName; //for error reporting

};

class ShaderProgram
{

public:
	ShaderProgram();

	void AttachShader(const Shader& shader);

	bool Init();
	bool Create();

	void Use() const;
	void Release() const;

	GLuint const GetUniformLocation(const std::string& name) const;

private:
	GLuint m_Program;

};

struct DefaultShaders
{
	static const std::string VertexDefault;
	static const std::string FragmentDefault;
};

}