
#pragma once

#include "ArrayBuffer.h"
#include "ElementBuffer.h"

namespace CGFramework
{

class Mesh
{

friend class Object;

public:
	Mesh();

private:
	ArrayBuffer m_ArrayBuffer;
	ElementBuffer m_ElementBuffer;

	GLuint m_MaterialIndex;

	void AddArrayAttrib(const GenericAttrib& attrib);
};

}
