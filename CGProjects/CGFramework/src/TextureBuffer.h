
#pragma once

#include "Buffer.h"
#include "IO.h"

namespace CGFramework
{

enum TexType
{
	Texture1D = GL_TEXTURE_1D,
	Texture2D = GL_TEXTURE_2D,
	Texture3D = GL_TEXTURE_3D
};

class TextureBuffer : public Buffer
{

public:
	TextureBuffer(GLuint samplerUniform);

	bool Init(TexType type, const char* texFile, GLuint texUnit = 0);

	inline void Bind() const;
	inline void Unbind() const;

	const GLuint GetTexUnit() const;

private:
	TexType m_TexType;
	GLuint m_TexUnit;
	GLuint m_SamplerUniform;
	GLuint m_SamplerHandle;

};

}