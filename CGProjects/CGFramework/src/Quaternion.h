
#pragma once

#include "Matrix.h"
#include "Vectors.h"

namespace CGFramework
{

namespace Math
{

struct Quaternion
{
	union
	{
		struct
		{
			float x, y, z, w;
		};

		float vec[4];
	};

	Quaternion();
	Quaternion(const Vector3& axis, float angleDeg);

	void Set(const Vector3& axis, float angleDeg);
	void Set(const Matrix4&);
	void Normalize();

	Matrix4 GetRotationMatrix() const;

	static Quaternion Conjugate(const Quaternion&);
	static Quaternion Inverse(const Quaternion&);

	static float Dot(const Quaternion&, const Quaternion&);
	static Quaternion Cross(const Quaternion&, const Quaternion&);

	static Quaternion Interpolate(const Quaternion& q1, const Quaternion& q2, const float& a);
};

Quaternion operator+ (const Quaternion&, const Quaternion&);
Quaternion operator* (const Quaternion&, const Quaternion&);
Quaternion operator* (const Quaternion&, const float& scalar);
Quaternion operator/ (const Quaternion&, const float& scalar);

}

}