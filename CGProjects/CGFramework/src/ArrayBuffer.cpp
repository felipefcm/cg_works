
#include "precompiled.h"

#include "ArrayBuffer.h"

namespace CGFramework
{

ArrayBuffer::ArrayBuffer()
{
}

bool ArrayBuffer::Init(const void* data, unsigned int dataSize, GLenum usageHint)
{
	if(data == NULL || dataSize == 0)
		return false;

	glGenBuffers(1, &m_BufferHandle);

	Bind();

	glBufferData(GL_ARRAY_BUFFER, dataSize, data, usageHint);
	
	Unbind();

	return true;
}

void ArrayBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_BufferHandle);
}

void ArrayBuffer::Unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ArrayBuffer::AddAttrib(const GenericAttrib& attrib)
{
	m_Attribs.push_back(attrib);
}

}
