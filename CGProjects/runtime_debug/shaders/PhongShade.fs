
#version 330

in vec3 transformedNormal;
in vec4 worldPosition;

out vec4 outputColor;

uniform vec3 lightPos = vec3(0, 2, 0); //in world space
uniform mat4 viewMatrixInverse;

uniform float KDiffuse = 0.7;
uniform vec4 diffuseColor = vec4(0.5, 0, 0, 1.0);

uniform float KAmbient = 0.1;
uniform vec4 ambientColor = vec4(0.5, 0, 0, 1.0);

uniform vec4 specularColor = vec4(1.0, 1.0, 1.0, 1.0);
uniform float KSpecular = 0.2;
uniform float materialShininess = 6;

uniform float attFactor = 0.1;

void main()
{
	vec3 normal = normalize(transformedNormal);
	vec3 viewDirection = vec3(normalize( viewMatrixInverse * vec4(0, 0, 0, 1.0) - worldPosition ));
	
	vec3 vectorToLight = lightPos - vec3(worldPosition);
	vec3 dirToLight = normalize(vectorToLight);
	
	float distance = length(vectorToLight);
	
	float attenuation = 1 / (attFactor * distance);

	float cosAngle = max(0.0, dot(normal, dirToLight));
	
	float cosViewAngle = pow(max(0.0, dot(reflect(-dirToLight, normal), viewDirection)), materialShininess);
	
	vec4 ambientLight = KAmbient * ambientColor;
	vec4 diffuseLight = KDiffuse * attenuation * diffuseColor * cosAngle;
	vec4 specularLight = KSpecular * attenuation * specularColor * cosViewAngle;

    outputColor = ambientLight + diffuseLight + specularLight;
	
};


