
#include "precompiled.h"

#include "TestApp.h"
#include "IO.h"

using namespace CGFramework;

TestApp::TestApp()
{
}

bool TestApp::Init()
{
	if(!Application::Init("TestApp", 800, 800))
		return false;

	glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
	//glPolygonMode(GL_BACK, GL_LINE);

	glEnable(GL_DEPTH_TEST);

	IO::Print("TestApp initializing...");

	glClearColor(0.1f, 0.1f, 0.1f, 0);

	Shader vertexShader, fragmentShader;

	IO::Print("	creating shaders");

	bool shaderError = false;

	if(!vertexShader.CreateFromFile(Shader::VERTEX, "shaders/PhongShade.vs") ||
	   !fragmentShader.CreateFromFile(Shader::FRAGMENT, "shaders/PhongShade.fs"))
			shaderError = true;

	m_PhongShader.Init();
	m_PhongShader.AttachShader(vertexShader);
	m_PhongShader.AttachShader(fragmentShader);

	if(!m_PhongShader.Create())
		shaderError = true;

	if(!vertexShader.CreateFromFile(Shader::VERTEX, "shaders/PhongShadeTex.vs") ||
	   !fragmentShader.CreateFromFile(Shader::FRAGMENT, "shaders/PhongShadeTex.fs"))
			shaderError = true;

	m_PhongShaderTex.Init();
	m_PhongShaderTex.AttachShader(vertexShader);
	m_PhongShaderTex.AttachShader(fragmentShader);

	if(!m_PhongShaderTex.Create())
		shaderError = true;

	vertexShader.Delete();
	fragmentShader.Delete();

	if(shaderError)
		return false;
	else
		IO::ShaderLogStream << "No shader errors/warnings";

	IO::CheckGLError();

	IO::Print("	preparing uniform values");

	int s_worldToViewMatrixUniform = m_PhongShader.GetUniformLocation("worldToViewMatrix");
	int s_projectionMatrixUniform = m_PhongShader.GetUniformLocation("projectionMatrix");
	int s_viewInverseMatrixUniform = m_PhongShader.GetUniformLocation("viewMatrixInverse");

	int t_worldToViewMatrixUniform = m_PhongShaderTex.GetUniformLocation("worldToViewMatrix");
	int t_projectionMatrixUniform = m_PhongShaderTex.GetUniformLocation("projectionMatrix");
	int t_viewInverseMatrixUniform = m_PhongShaderTex.GetUniformLocation("viewMatrixInverse");

	IO::CheckGLError();

	//Setting up a perspective projection matrix
	m_ProjectionMatrix = Math::Matrix4::StdPerspectiveMatrix(PERSPECTIVE_FOV, 1.0f, 1.0f, 100.0f);

	//Setting up camera
	m_Camera.SetEye(Vector3(0, 0, 0));
	m_Camera.SetCenter(Vector3(0, 0, -1.0f));
	m_Camera.SetUp(Vector3(0, 1.0f, 0));

	IO::Print("	assigning some uniform values");

	m_PhongShader.Use();
	
	glUniformMatrix4fv(s_projectionMatrixUniform, 1, GL_TRUE, m_ProjectionMatrix.matrix_a);
	glUniformMatrix4fv(s_worldToViewMatrixUniform, 1, GL_TRUE, m_Camera.GetViewMatrix().matrix_a);
	glUniformMatrix4fv(s_viewInverseMatrixUniform, 1, GL_TRUE, Matrix4::Invert(m_Camera.GetViewMatrix()).matrix_a);

	m_PhongShader.Release();

	m_PhongShaderTex.Use();
	
	glUniformMatrix4fv(t_projectionMatrixUniform, 1, GL_TRUE, m_ProjectionMatrix.matrix_a);
	glUniformMatrix4fv(t_worldToViewMatrixUniform, 1, GL_TRUE, m_Camera.GetViewMatrix().matrix_a);
	glUniformMatrix4fv(t_viewInverseMatrixUniform, 1, GL_TRUE, Matrix4::Invert(m_Camera.GetViewMatrix()).matrix_a);

	m_PhongShaderTex.Release();

	//objects
	IO::Print("	loading models");

	Object minecraftObj(m_PhongShaderTex);
	Object cubo(m_PhongShader);
	Object esfera(m_PhongShader);
	Object plano(m_PhongShader);

	if(!minecraftObj.LoadModel("models/minecraft.obj"))
		return false;

	if(!cubo.LoadModel("models/cubo.obj"))
		return false;

	if(!esfera.LoadModel("models/esfera.obj"))
		return false;

	if(!plano.LoadModel("models/plano.obj"))
		return false;

	m_Objects.push_back(minecraftObj); //0
	m_Objects.push_back(cubo);      //1
	m_Objects.push_back(esfera);    //2
	m_Objects.push_back(plano);		//3

	//Setting the SceneGraph
	character = LeafNode(m_Objects[0]);

	shoulderSphere.SetObject(m_Objects[2]);
	upperArm.SetObject(m_Objects[1]);
	elbowSphere.SetObject(m_Objects[2]);
	lowerArm.SetObject(m_Objects[1]);
	wristSphere.SetObject(m_Objects[2]);
	hand.SetObject(m_Objects[1]);
	plane.SetObject(m_Objects[3]);

	//Setting hierarchy
	m_SceneGraph.SetRoot(&scene);

	scene.AddChild(arm);
	scene.AddChild(minecraft);
	scene.AddChild(plane);

	arm.AddChild(shoulder);

	minecraft.AddChild(character);

	shoulder.AddChild(shoulderSphere);
	shoulder.AddChild(upperArm);
	shoulder.AddChild(elbow);

	elbow.AddChild(elbowSphere);
	elbow.AddChild(lowerArm);
	elbow.AddChild(wrist);

	wrist.AddChild(wristSphere);
	wrist.AddChild(hand);

	//Setting transformations
	scene.SetTransform(Matrix4::TranslateMatrix(0, 0, -5.0f));

	plane.SetTransform(
		Matrix4::TranslateMatrix(0, -1.0f, 0) *
		Matrix4::ScaleMatrix(5.0f, 5.0f, 5.0f)
		);

	arm.SetTransform(
		Matrix4::ScaleMatrix(0.3f, 0.3f, 0.3f) *
		Matrix4::TranslateMatrix(-6.0f, 0, 0)
		);

	minecraft.SetTransform(
		Matrix4::ScaleMatrix(0.6f, 0.6f, 0.6f) *
		Matrix4::TranslateMatrix(2.0f, -1.5f, 0)
		);

	shoulder.SetTransform(
		Matrix4::RotateMatrix(-45.0f, 0, 0, 1.0f)
		);

	shoulderSphere.SetTransform(
		Matrix4::ScaleMatrix(0.6f, 0.6f, 0.6f)
		);

	upperArm.SetTransform(
		Matrix4::TranslateMatrix(1.9f, 0, 0) *
		Matrix4::ScaleMatrix(1.4f, 0.3f, 0.3f)
		);

	elbow.SetTransform(
		Matrix4::TranslateMatrix(3.2f, 0, 0) *
		Matrix4::RotateMatrix(90.0f, 0, 0, 1.0f)
		);

	elbowSphere.SetTransform(
		Matrix4::ScaleMatrix(0.45f, 0.45f, 0.45f)
		);

	lowerArm.SetTransform(
		Matrix4::TranslateMatrix(1.5f, 0, 0) *
		Matrix4::ScaleMatrix(1.2f, 0.25f, 0.25f)
		);

	wrist.SetTransform(
		Matrix4::TranslateMatrix(2.8f, 0, 0) *
		Matrix4::RotateMatrix(-75.0f, 0, 0, 1.0f)
		);

	wristSphere.SetTransform(
		Matrix4::ScaleMatrix(0.3f, 0.3f, 0.3f)
		);

	hand.SetTransform(
		Matrix4::TranslateMatrix(0.6f, 0, 0) *
		Matrix4::ScaleMatrix(0.4f, 0.15f, 0.4f)
		);

	IO::Print("TestApp initialized");

	return true;
}

void TestApp::VUpdate()
{
}

void TestApp::VRender() 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_SceneGraph.Render();

	glutSwapBuffers();
}

void TestApp::VCleanup()
{
}

void TestApp::VResize(int w, int h)
{
	//Resize for perspective projection
	m_ProjectionMatrix.matrix[0][0] = (1 / tanf(PERSPECTIVE_FOV / 2)) / (w / h);

	/*
	m_TextureShader.Use();

	glUniformMatrix4fv(m_TextureShader.GetUniformLocation("projectionMatrix"),
			1, GL_TRUE, m_ProjectionMatrix.matrix_a);

	m_TextureShader.Release();
	*/

	m_PhongShader.Use();

	glUniformMatrix4fv(m_PhongShader.GetUniformLocation("projectionMatrix"),
			1, GL_TRUE, m_ProjectionMatrix.matrix_a);

	m_PhongShader.Release();

	glViewport(0, 0, w, h);
}

void TestApp::VKeyboardEvent(unsigned char key, int x, int y)
{
	Quaternion armMod, shoulderMod, elbowMod, wristMod;
	Quaternion charMod;

	float angle = 15.0f; 

	switch(key)
	{
		/* arm controls */
		case 'a': 
			armMod.Set(Vector3(0, 1.0f, 0), angle);
		break;

		case 'd': 
			armMod.Set(Vector3(0, 1.0f, 0), -angle);
		break;

		case 'w': 
			shoulderMod.Set(Vector3(0, 0, 1.0f), angle);
		break;

		case 's': 
			shoulderMod.Set(Vector3(0, 0, 1.0f), -angle);
		break;

		case 'q': 
			elbowMod.Set(Vector3(0, 0, 1.0f), angle);
		break;

		case 'e': 
			elbowMod.Set(Vector3(0, 0, 1.0f), -angle);
		break;

		case 'r':
			wristMod.Set(Vector3(0, 0, 1.0f), angle);
		break;

		case 'f':
			wristMod.Set(Vector3(0, 0, 1.0f), -angle);
		break;

		/* character controls */
		case 'j':
			charMod.Set(Vector3(0, 1.0f, 0), angle);
		break;

		case 'l':
			charMod.Set(Vector3(0, 1.0f, 0), -angle);
		break;

		default:
			return;
	}

	arm.SetTransform(arm.GetTransform() * armMod.GetRotationMatrix());
	shoulder.SetTransform(shoulder.GetTransform() * shoulderMod.GetRotationMatrix());
	elbow.SetTransform(elbow.GetTransform() * elbowMod.GetRotationMatrix());
	wrist.SetTransform(wrist.GetTransform() * wristMod.GetRotationMatrix());
	character.SetTransform(character.GetTransform() * charMod.GetRotationMatrix());
}

void TestApp::VMouseEvent(int button, int state, int x, int y)
{
}
