
#include "precompiled.h"

#include "TestApp.h"

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	TestApp app;

	return (app.Init() && app.Run() == 0 ? 0 : -1);
}