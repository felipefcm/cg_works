
#pragma once

#include "Application.h"
#include "Object.h"
#include "Shader.h"
#include "StdMath.h"
#include "MatrixStack.h"
#include "Camera.h"
#include "SceneGraph.h"

#define		PERSPECTIVE_FOV		45.0f

using namespace CGFramework;
using namespace Math;

class TestApp : public Application
{

public:

	TestApp();

	bool Init();

	void VUpdate();

	void VRender();

	void VCleanup(); 

	void VResize(int w, int h);

	void VKeyboardEvent(unsigned char key, int x, int y);
	void VMouseEvent(int button, int state, int x, int y);

private:
	std::vector<Object> m_Objects;

	//ShaderProgram m_FlatShader;
	//ShaderProgram m_GouraudShader;

	ShaderProgram m_PhongShader;
	ShaderProgram m_PhongShaderTex;

	Matrix4 m_ProjectionMatrix;
	MatrixStack m_MatrixStack;

	SceneGraph m_SceneGraph;

	Camera m_Camera;

	//nodes are not following framework naming conventions due some reason
	GroupNode scene;
	GroupNode arm;
	GroupNode minecraft;

	GroupNode shoulder;
	GroupNode elbow;
	GroupNode wrist;

	LeafNode character;

	LeafNode plane;

	LeafNode shoulderSphere;
	LeafNode upperArm;
	LeafNode elbowSphere;
	LeafNode lowerArm;
	LeafNode wristSphere;
	LeafNode hand;

};
